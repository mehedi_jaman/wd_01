<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
	<style>
		#LoginArea{
			margin-top: 200px;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row" id="LoginArea">
			<div class="col-md-4 col-md-offset-4">
				<form action="" class="form-horizontal">
					<div class="form-group">
						<label for="Email">Email</label>
						<input type="email" name="Email" class="form-control">
					</div>

					<div class="form-group">
						<label for="Password">Password</label>
						<input type="password" name="Password" class="form-control">
					</div>

					<div class="form-group">
						<input type="submit" name="submit" value="Login" class="btn btn-primary btn-block">
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row" id="LoginArea">
			<div class="col-md-4 col-md-offset-4">
				<form action="" class="form-horizontal">
					<div class="form-group"><label for="">Email</label><input type="text" class="form-control"></div>

					<div class="form-group"><label for="">Password</label><input type="text" class="form-control"></div>
					
					<div class="form-group"><input type="text" class="btn btn-primary btn-block" value="Login"></div>
				</form>
			</div>
		</div>
	</div>

	<!-- <div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat modi ipsam quibusdam molestiae consectetur nisi nobis aperiam doloribus esse, magni neque totam ipsa illum reprehenderit mollitia. Odit delectus consequatur repellendus!
			</div>
			<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
				Lorem ipsum dolor sit amet consectetur adipisicing, elit. Ipsa cumque ut incidunt, sed! Quibusdam quidem mollitia facere vel sint voluptatibus vitae consequuntur ipsum nemo, modi maxime, explicabo sequi exercitationem, deleniti.
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
				Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit consectetur corporis eum nam totam? Aliquid impedit, nihil nobis itaque sed consequuntur tempore sequi quae totam, qui explicabo omnis, quis magni.
			</div>
			<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa placeat, quae enim a qui laborum quisquam. Veritatis quis fuga culpa placeat tenetur officiis, possimus pariatur corrupti vel rem sunt inventore!
			</div>
			<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
				Lorem ipsum dolor sit amet consectetur adipisicing, elit. Impedit officiis aspernatur corporis animi cupiditate, alias maxime modi, quasi eligendi debitis cumque dolores, iusto eveniet magnam possimus officia tempora nobis neque!
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
				Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident accusamus dignissimos quidem, illo nam iste iure ad doloribus architecto, optio saepe tempore, eius veritatis repellendus dolorem temporibus esse ipsam rerum!
			</div>
			<div class="col-md-10 col-lg-10 col-sm-10 col-xs-10">
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam aliquam, porro cum sint, alias hic quas atque rerum velit accusantium nostrum impedit voluptatum sit est eligendi laborum tempore dolorem quia.
			</div>
		</div>
	</div> -->

	<!-- <div class="cotainer">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				Lorem ipsum dolor sit, amet consectetur, adipisicing elit. Similique officia illum deserunt ad tempore quidem consectetur dolores necessitatibus quis? Odit aliquid totam veniam repellendus dolorem est quaerat quisquam doloribus similique!
			</div>
		</div>
	</div> -->

	<!-- <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
		Lorem ipsum dolor sit amet consectetur adipisicing, elit. Tempore corrupti quisquam soluta, minus ut, exercitationem placeat qui doloribus! Reprehenderit provident nulla, quis in! Dignissimos fuga veniam totam error reiciendis odit.
	</div>

	<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
		Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Ullam provident fuga nemo consequatur assumenda at impedit velit earum, corrupti perferendis expedita ab ut quos! Earum non assumenda enim, voluptate natus.
	</div>

	<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
		Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Sapiente voluptas placeat provident itaque nulla unde sit odit iste, hic nemo minima ea! Animi, assumenda omnis. Ullam expedita fugiat dignissimos nemo.
	</div>

	<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
		Lorem ipsum dolor sit amet, consectetur adipisicing, elit. Cumque eos, laborum nemo accusamus ducimus debitis? Officia repudiandae fugiat quos blanditiis, rem veniam, iusto temporibus numquam debitis delectus, libero optio atque.
	</div>

	<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
		Lorem ipsum, dolor sit amet consectetur adipisicing elit. Similique aliquid accusamus, enim saepe, qui adipisci facilis quidem consectetur, iusto possimus dolorem hic a explicabo inventore corporis molestias impedit ad, aliquam!
	</div>

	<div class="col-md-3">
		Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Totam accusamus laudantium, aspernatur? Magni similique quia, nulla vero quod maxime iste minus aliquam eos, qui nostrum sunt vitae laboriosam praesentium aspernatur!
	</div>

	<div class="col-md-3">
		Lorem, ipsum dolor sit amet consectetur adipisicing elit. Asperiores in corporis numquam nesciunt error. Id adipisci labore sunt, eius cumque pariatur. Deleniti eveniet, dicta nihil, necessitatibus repudiandae quos harum. Quidem!
	</div>

	<div class="col-md-3">
		Lorem ipsum, dolor sit amet consectetur adipisicing elit. Excepturi necessitatibus in libero laudantium eaque distinctio. A nesciunt magnam hic sunt necessitatibus repudiandae accusantium modi nam, eius vitae blanditiis. Cum, molestias.
	</div>

	<div class="col-md-3">
		Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem, enim, architecto. Nostrum libero sed id rerum natus totam deleniti dolorum illo, optio dicta quas aspernatur rem itaque beatae tempore iusto?
	</div> -->

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>