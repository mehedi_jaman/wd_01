<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>KPI Website</title>
	<link rel="stylesheet" href="libs/bootstrap-3.4.1/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="libs/fontawesome-free-5.15.3-web/css/fontawesome.min.css">
	<link rel="stylesheet" href="libs/fontawesome-free-5.15.3-web/css/solid.min.css">
	<link rel="stylesheet" href="libs/fontawesome-free-5.15.3-web/css/brands.min.css">
	<link rel="stylesheet" href="css/custom.css">
</head>
<body>
	
	<script src='libs/jquery-3.6.0.min.js'></script>
	<script src="libs/bootstrap-3.4.1/dist/js/bootstrap.min.js"></script>
</body>
</html>