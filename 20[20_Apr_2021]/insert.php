<?php 
	include('db.php');


	if (isset($_POST['submit'])) {
		if ($_POST['submit'] == 'Store Information') {
			$Name 		= $_POST['Name'];
			$ClassRoll  = $_POST['ClassRoll'];
			$BoardRoll  = $_POST['BoardRoll'];
			$RegNo 		= $_POST['RegNo'];
			$Session  	= $_POST['Session'];
			$Semester   = $_POST['Semester'];
			$Department = $_POST['Department'];

			$Query = "SELECT * FROM StudentList WHERE ClassRoll = '$ClassRoll'";

			$Result = mysqli_query($Connection, $Query);

			$Count = mysqli_num_rows($Result);

			if ($Count > 0) {
				$ErrorMsg =  "This Class Roll already exists";
			}
			else{
				$Query = "INSERT  INTO StudentList (Name,ClassRoll,BoardRoll,RegNo,Session,Semester,Department)";
				$Query .= "VALUES('$Name','$ClassRoll','$BoardRoll','$RegNo','$Session','$Semester','$Department')";

				$Result  = mysqli_query($Connection,$Query);

				if (!$Result) {
					$ErrorMsg =  "Something Error in Query Execution !";
				}
				else
					$SuccessMsg = "Student Created Successfully !";
			}
		}
	}



	$Query = "SELECT * FROM StudentList";
	$Result = mysqli_query($Connection,$Query);	
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Insert</title>
</head>
<body>
	<?php 
		if (isset($SuccessMsg)) {
			echo $SuccessMsg;
		}

		if (isset($ErrorMsg)) {
			echo $ErrorMsg;
		}
	 ?>


	<h1 align="center">Data Insert</h1>

	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="Name">Name</label>
		<input type="text" name="Name"> <br>

		<label for="ClassRoll">Class Roll</label>
		<input type="text" name="ClassRoll"> <br>

		<label for="BoardRoll">Board Roll</label>
		<input type="text" name="BoardRoll"> <br>

		<label for="RegNo">Reg No</label>
		<input type="text" name="RegNo"> <br>

		<label for="Session">Session</label>
		<input type="text" name="Session"> <br>

		<label for="Semester">Semester</label>
		<select name="Semester">
			<option value="">Select Semester</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
		</select> <br>

		<label for="Department">Department</label>
		<select name="Department">
			<option value="">Select Department</option>
			<option value="CMT">CMT</option>
			<option value="CT">CT</option>
			<option value="MT">MT</option>
			<option value="ENT">ENT</option>
			<option value="ET">ET</option>
			<option value="MRT">MRT</option>
			<option value="AIDT">AIDT</option>
		</select> <br>
 
		<input type="submit" name="submit" value="Store Information">
	</form>


	<table border="1px solid black">
		<thead>
			<tr>
				<th>Serial</th>
				<th>Name</th>
				<th>Class Roll</th>
				<th>Board Roll</th>
				<th>Reg No</th>
				<th>Session</th>
				<th>Semester</th>
				<th>Department</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			<?php while($Row = mysqli_fetch_array($Result, MYSQLI_ASSOC)) {?>
				<tr>
					<td><?php echo $Row['Serial']; ?></td>
					<td><?php echo $Row['Name']; ?></td>
					<td><?php echo $Row['ClassRoll']; ?></td>
					<td><?php echo $Row['BoardRoll']; ?></td>
					<td><?php echo $Row['RegNo']; ?></td>
					<td><?php echo $Row['Session']; ?></td>
					<td><?php echo $Row['Semester']; ?></td>
					<td><?php echo $Row['Department']; ?></td>
					<td>
						<a href="view.php?Action=View&Serial=<?php echo $Row['Serial']; ?>">View</a>
						<a href="edit.php?Action=Edit&Serial=<?php echo $Row['Serial']; ?>">Edit</a>
						<a href="delete.php?Action=Delete&Serial=<?php echo $Row['Serial']; ?>">Delete</a>
					</td>
				</tr>
			<?php } ?>

		</tbody>
	</table>
</body>
</html>