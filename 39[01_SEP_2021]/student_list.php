<?php 
    include('db.php');

    $Query = "SELECT * FROM students";

    $Result = mysqli_query($Connection,$Query);

 ?>
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Student List</h1>
            
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Class Roll</th>
                                <th>Board Roll</th>
                                <th>Reg No</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                            
                        <tbody>
                            <?php 

                                while($Row = mysqli_fetch_array($Result,MYSQLI_ASSOC)){
                            ?>
                            <tr>
                                <td> <?php echo $Row['name']; ?> </td>
                                <td> <?php echo $Row['class_roll']; ?> </td>
                                <td> <?php echo $Row['board_roll']; ?> </td>
                                <td> <?php echo $Row['reg_no']; ?> </td>
                                <td> <?php echo $Row['phone']; ?> </td>
                                <td>
                                    <a href="" class="btn btn-warning">Edit</a>
                                    <a href="" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->