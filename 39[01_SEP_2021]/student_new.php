<?php 
    include('db.php');

    if (isset($_POST['submit'])) {
        if ($_POST['submit'] == 'Add new Student') {
            $name       =  $_POST['name'];
            $class_roll = $_POST['class_roll'];
            $board_roll = $_POST['board_roll'];
            $reg_no     = $_POST['reg_no'];
            $phone      = $_POST['phone'];

            $Query = "INSERT INTO students (name,class_roll,board_roll,reg_no,phone) VALUES('$name','c$class_roll','$board_roll','$reg_no','$phone')";

            $Result = mysqli_query($Connection,$Query);

            if(!$Result){
                $ErrorMsg = 'Data insertion Failed !';
            }
            else
                $SuccessMsg = 'New Student Added Successfully !';
        }
    }

 ?>
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">New Student</h1>
                        <?php 
                            if (isset($ErrorMsg)) {
                                echo $ErrorMsg;
                            }

                            if (isset($SuccessMsg)) {
                                echo $SuccessMsg;
                            }
                         ?>
                    </div>

                    <!-- Content Row -->
                    <div class="row">
                        <form class="user" action="<?php echo $_SERVER['SELF']; ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-user"
                                    placeholder="Student Name" name="name">
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="text" class="form-control form-control-user"
                                        id="exampleInputPassword" placeholder="Class Roll" name="class_roll">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user"
                                        id="exampleRepeatPassword" placeholder="Board Roll" name="board_roll">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="text" class="form-control form-control-user"
                                        id="exampleInputPassword" placeholder="Reg No" name="reg_no">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user"
                                        id="exampleRepeatPassword" placeholder="Phone" name="phone">
                                </div>
                            </div>
                            <input type="submit" name="submit" class="btn btn-primary btn-user btn-block" value="Add new Student">
                        </form>
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->