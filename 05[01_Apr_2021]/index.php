<?php 
	session_start();

	if ($_SESSION['Status'] != 1) {
		header('location:login.php');
	}
	
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>This is First Page</h1>

	<p>
		Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit nisi velit id voluptatum placeat assumenda eveniet aut, eaque amet laboriosam ad et, aliquam dolor fuga ut odit officia ratione consectetur.

		Lorem ipsum dolor sit amet consectetur adipisicing, elit. Iusto quis voluptatem non impedit necessitatibus nemo ipsam autem, inventore officiis laboriosam veniam quasi, expedita quo, ducimus aspernatur praesentium repudiandae asperiores at.

		Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolorum quidem odit magni beatae natus alias repellendus inventore sed ipsa cum, veritatis ea numquam ipsum vel necessitatibus animi aperiam optio quia.
	</p>

	<a href="logout.php">Logout</a>
</body>
</html>