<?php 
	session_start();

	if ($_SESSION['Status'] != 1) {
		header('location:index.php');
	}
	echo $_SESSION['FirstName'];


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>This is a View Page </h1>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus repellendus eum, quisquam perferendis dolorum ut deserunt excepturi. Aperiam, ducimus, veritatis. Non ullam cumque fugiat modi et magnam impedit eius quia.

		Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Voluptas facere expedita deleniti fuga sint deserunt, ipsam nulla necessitatibus ipsum eos fugit omnis aliquid distinctio, rem, totam a suscipit quisquam dolorem?

		Lorem ipsum dolor, sit amet, consectetur adipisicing elit. Ipsum id blanditiis aspernatur, tempore nesciunt dolor, impedit ducimus magni explicabo cum repudiandae dolorum. Eos blanditiis quod, sint rerum labore ex, illo!
	</p>

	<a href="logout.php">Logout</a>
</body>
</html>