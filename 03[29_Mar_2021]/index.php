<?php

	if (isset($_GET['submit'])) {
		$FirstName = $_GET['FirstName'];
		$LastName  = $_GET['LastName'];

		$FullName = $FirstName.' '.$LastName;

		echo 'Your Full Name is '.$FullName;
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET" enctype="multipart/form-data">
		<label for="FirstName">First Name:</label>
		<input type="text" name="FirstName">

		<label for="LastName">Last Name:</label>
		<input type="text" name="LastName">

		<input type="submit" name="submit" value="View Full Name">
	</form>
</body>
</html>