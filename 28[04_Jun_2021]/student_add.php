<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Add Student</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">WEB</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="/">Home</a></li>
	        <li><a href="logout.php">logout</a></li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="row">
		<div class="container">
			<div class="col-md-6 col-md-offset-3">
				<h3 class="text-center">Add Student</h3>
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST" class="form-horizontal">
					<div class="form-group">
						<label for="Name" class="control-label col-md-3">Name</label>
						<div class="col-md-9">
							<input type="text" name="Name" id="Name" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label for="ClassRoll" class="control-label col-md-3">Class Roll</label>

						<div class="col-md-3">
							<input type="text" name="ClassRoll" id="ClassRoll" class="form-control">
						</div>

						<label for="BoardRoll" class="control-label col-md-3">Board Roll</label>

						<div class="col-md-3">
							<input type="text" name="BoardRoll" id="BoardRoll" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label for="RegNo" class="control-label col-md-3">Reg No</label>

						<div class="col-md-3">
							<input type="text" name="RegNo" id="RegNo" class="form-control">
						</div>

						<label for="Session" class="control-label col-md-3">Session</label>

						<div class="col-md-3">
							<input type="text" name="Session" id="Session" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label for="Semester" class="control-label col-md-3">Semester</label>

						<div class="col-md-3">
							<select name="Semester" id="Semester" class="form-control">
								<option value="">Select Semester</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
							</select>
						</div>

						<label for="Department" class="control-label col-md-3">Department</label>

						<div class="col-md-3">
							<select name="Department" id="Department" class="form-control">
								<option value="">Select Department</option>
								<option value="CMT">CMT</option>
								<option value="CT">CT</option>
								<option value="MRT">MRT</option>
								<option value="ET">ET</option>
								<option value="ENT">ENT</option>
								<option value="MT">MT</option>
								<option value="AIDT">AIDT</option>
								<option value="TT">TT</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="Photo" class="control-label col-md-3">Photo</label>

						<div class="col-md-9">
							<input type="file" name="Photo" id="Photo" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-4 col-md-offset-8">
							<input type="submit" name="submit" value="Add Student" class="btn btn-primary btn-block">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>