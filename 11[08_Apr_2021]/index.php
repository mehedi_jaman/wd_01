<?php 
	session_start();
	if ($_SESSION['Status'] != 1) {
		header('location:login.php');
	}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web Development Class</title>
</head>
<body>
	<h1>Web Development Class</h1>
	<p>
		Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur temporibus deleniti, ex porro vero, aut odio nam, distinctio nobis sapiente earum voluptatibus. Fugiat provident ab officiis placeat itaque ut nostrum.
	</p>

	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, quo quae, saepe vitae ad nesciunt, veniam unde tenetur ipsa amet nihil excepturi. Veniam natus, adipisci atque nihil et quis quos.
	</p>

	<a href="logout.php">Logout</a>
</body>
</html>