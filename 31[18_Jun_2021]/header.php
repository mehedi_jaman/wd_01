<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>KPI Website</title>
	<link rel="stylesheet" href="libs/bootstrap-3.4.1/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="libs/fontawesome-free-5.15.3-web/css/fontawesome.min.css">
	<link rel="stylesheet" href="libs/fontawesome-free-5.15.3-web/css/solid.min.css">
	<link rel="stylesheet" href="libs/fontawesome-free-5.15.3-web/css/brands.min.css">
	<link rel="stylesheet" href="css/custom.css">
</head>
<body>	
	<div class="container">
		<!-- Header -->
		<div class="row">
			<nav class="header">
			  <div class="container-fluid">
			  	<span><i class="fas fa-custom fa-map-marker"></i> খালিশপুর, খুলনা-৯০০০ <i class="fas fa-custom fa-phone"></i>+৮৮০৪১-৭৬২৩৫২</span> 

			  	<div class="pull-right">
			  		<a href=""><strong>Login</strong></a>
			  		<a href=""><strong>Register</strong></a>
			  	</div>
			  </div>
			</nav>
		</div>
		<!-- Slider -->
		<div class="row">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			      <img src="images/slider/1.png" alt="Slider Image 1" class="img img-responsive SliderImage">
			      <div class="carousel-caption">
			        <h1 class="text-center">Khulna Polytechnic Insitute</h1>
			      </div>
			    </div>
			    <div class="item">
			      <img src="images/slider/2.jpg" alt="Slider Image 2" class="img img-responsive SliderImage">
			      <div class="carousel-caption">
			        <h1 class="text-center">Khulna Polytechnic Insitute</h1>
			      </div>
			    </div>
			    <div class="item">
			      <img src="images/slider/2.jpg" alt="Slider Image 3" class="img img-responsive SliderImage">
			      <div class="carousel-caption">
			        <h1 class="text-center">Khulna Polytechnic Insitute</h1>
			      </div>
			    </div>
			    
			  </div>

			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
		<!-- Menubar -->
		<div class="row">
			<nav class="navbar navbar-custom">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			   <!--  <div class="navbar-header">
			      <button type="button" class
			        <li class="active"><a hre="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">Brand</a>
			    </div> -->

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav">
			        <li class="active"><a href="index.php"><i class="fa fa-home"></i></a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">আমাদের সম্পর্কে <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="index.php?page=about_institute">প্রতিষ্ঠান সম্পর্কে</a></li>
			            <li><a href="index.php?page=about_mission">লক্ষ্য (রুপকল্প)</a></li>
			            <li><a href="#">উদ্দেশ্য (অভিলক্ষ্য)</a></li>
			            <li><a href="index.php?page=principal_message">অধ্যক্ষ মহোদয়ের বাণী</a></li>
			            <li><a href="#">উপাধ্যক্ষ মহোদয়ের বাণী</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">টেকনোলজি <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="#">সিভিল</a></li>
			            <li><a href="#">ইলেকট্রিক্যাল</a></li>
			            <li><a href="#">মেকানিক্যাল</a></li>
			            <li><a href="#">পাওয়ার</a></li>
			            <li><a href="#">ইলেকট্রনিক্স</a></li>
			            <li><a href="#">কম্পিউটার</a></li>
			            <li><a href="#">ইএনভিটি</a></li>
			            <li><a href="#">আইপিসিটি</a></li>
			            <li><a href="#">আরএসি</a></li>
			            <li><a href="#">আর এস</a></li>
			          </ul>
			        </li>
			        <li><a href="">ক্লাশ রুটিন</a></li>
			        <li><a href="">রেজাল্ট</a></li>
			        <li><a href="">ফটো গ্যালারি</a></li>
			        <li><a href="">NTVQF</a></li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NTVQF <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="#">লেভেল – ১</a></li>
			            <li><a href="#">লেভেল – ২</a></li>
			            <li><a href="#">লেভেল – ৩</a></li>
			            <li><a href="#">লেভেল – ৪</a></li>
			          </ul>
			        </li>
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">একাডেমিক <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="#">সিলেবাস</a></li>
			            <li><a href="#">একাডেমিক ক্যালেন্ডার</a></li>
			            <li><a href="#">রেজিষ্টার</a></li>
			            <li><a href="#">লাইব্রেরী</a></li>
			            <li><a href="#">জেনারেল সেকশন</a></li>
			            <li><a href="#">একাউন্টস</a></li>
			            <li><a href="#">কেয়ার টেকার</a></li>
			            <li><a href="#">ষ্টোর সেকশন</a></li>
			            <li><a href="#">সাপোর্ট ষ্টাফ</a></li>
			          </ul>
			        </li>
			        <li><a href="">যোগাযোগ</a></li>
			      </ul>		      
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div>