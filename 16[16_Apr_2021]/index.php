<?php 
	if (isset($_POST['submit'])) {
		$FileName = $_FILES['Image']['name'];
		$FileSize = $_FILES['Image']['size'];
		$FileTmp  = $_FILES['Image']['tmp_name'];
		$FileType = $_FILES['Image']['Type'];
		$FileExt  = strtolower(end(explode('.',$FileName)));

		if ($FileSize > 500120) {
			echo "Sorry Image Size exceeds !";
		}
		elseif($FileExt == 'jpg' || $FileExt == 'jpeg' || $FileExt == 'png' || $FileExt == 'bmp' || $FileExt == 'pdf'){
			$NewName = rand(100000,9999999).rand(100000,9999999).'.'.$FileExt;
			$TargetDir = 'images/'.$NewName;

			if (!move_uploaded_file($FileTmp, $TargetDir)) {
				echo "Sorry Something error ! Image upload failed.";
			}
			else
				echo "Successfully uploaded Image";
		}
		else{
			echo "Sorry This is not an Image, Please upload a valid Image";
		}

	}	
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP Image Upload</title>
</head>
<body>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="Image">Image</label>
		<input type="file" name="Image">

		<input type="submit" name="submit" value="Upload Photo">
	</form>
</body>
</html>