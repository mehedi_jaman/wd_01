<?php 
	include('sidebar.php');
	include('topbar.php');
	


	$page = $_REQUEST['page'];

	switch ($page) {
		case 'new_student':
			include('student_new.php');
			break;

		case 'student_list':
			include('student_list.php');
			break;
		
		default:
			include('dashboard.php');
			break;
	}

	include('footer.php');
 ?>