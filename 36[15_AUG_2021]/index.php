<?php

	include('header.php');
	include('sidebar.php');

	$page = $_REQUEST['page'];

	switch ($page) {
		case 'dashboard':
			include('home.php');
			break;

		case 'table':
			include('table.php');
			break;
		
		default:
			include('home.php');
			break;
	}	
	
	include('footer.php');
 ?>