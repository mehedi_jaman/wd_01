<?php 
	session_start();

	include('db.php');
	
	if ($_SESSION['Status'] != 1) {
		header('location:logout.php');
	}

	if (isset($_REQUEST['Success'])) {
		$SuccessMsg = $_REQUEST['Success'];
	}

	if (isset($_POST['submit'])) {
		if ($_POST['submit'] == 'Store Information') {
			$Name 		= $_POST['Name'];
			$ClassRoll  = $_POST['ClassRoll'];
			$BoardRoll  = $_POST['BoardRoll'];
			$RegNo 		= $_POST['RegNo'];
			$Session 	= $_POST['Session'];
			$Semester 	= $_POST['Semester'];
			$Department = $_POST['Department'];
			$Photo 		= '';

			$Query = "SELECT * FROM StudentList WHERE ClassRoll = '$ClassRoll'";

			$Result = mysqli_query($Connection,$Query);

			$Count = mysqli_num_rows($Result);

			if ($Count > 0) {
				$ErrorMsg = "Sorry ! This Class Roll already exists.";
			}
			else{

				if (isset($_FILES['Photo']['name'])) {
					if (!empty($_FILES['Photo']['name'])) {
						$FileName = $_FILES['Photo']['name'];
						$FileSize = $_FILES['Photo']['size'];
						$FileType = $_FILES['Photo']['type'];
						$FileTmp  = $_FILES['Photo']['tmp_name'];
						$FileExt  = strtolower(pathinfo($_FILES['Photo']['name'],PATHINFO_EXTENSION));

						if ($FileSize > 2097152) {
							$ErrorMsg = 'File size exceeds';
						}

						if ($FileExt == 'jpg' || $FileExt == 'png' || $FileExt == 'bmp' || $FileExt == 'jpeg') {
							$NewName = $ClassRoll.'_'.rand(9999,99999).'.'.$FileExt;
							$Destination = 'images/'.$NewName;

							if (!move_uploaded_file($FileTmp, $Destination)) {
								$ErrorMsg = 'File upload failed';
							}
							else{
								$Photo = $NewName;
							}			
						}
						else{
							$ErrorMsg = 'File Type not supported';
						}
					}
				}


				$Query = 'INSERT INTO StudentList (Name,ClassRoll,BoardRoll,RegNo,Session,Semester,Department,Photo)';
				$Query .= "VALUES('$Name','$ClassRoll','$BoardRoll','RegNo','$Session','$Semester','$Department','$Photo')";

				$Result = mysqli_query($Connection,$Query);

				if (!$Result) {
					$ErrorMsg = 'Query Execution Failed !';
				}
				else
					$SuccessMsg = "New Student Added Successfully !";
			}
		}
	}

	if (isset($_REQUEST['Action'])) {
		if ($_REQUEST['Action'] == 'Delete') {
			$Serial = $_REQUEST['Serial'];

			$Query = "DELETE FROM StudentList WHERE `Serial` = '$Serial'";

			$Result = mysqli_query($Connection,$Query);

			if (!$Result) {
				$ErrorMsg = 'Delete Failed';
			}
			else
				$SuccessMsg = 'Deleted Successfully !';
		}
	}


	$Query = "SELECT * FROM StudentList";
	$Result = mysqli_query($Connection,$Query);

	mysqli_close();
	
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Insert</title>
</head>
<body>
	<?php 
		if (isset($ErrorMsg)) {
			echo "<h4 align='center' style='color:red;'>".$ErrorMsg."</h4>";
		}

		if (isset($SuccessMsg)) {
			echo "<h4 align='center' style='color:green;'>".$SuccessMsg."</h4>";
		}
	 ?>
	<h1 align="center">Data Insert</h1>

	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="Name">Name</label>
		<input type="text" id="Name" name="Name"> <br>

		<label for="ClassRoll">Class Roll</label>
		<input type="text" id="ClassRoll" name="ClassRoll"> <br>

		<label for="BoardRoll">Board Roll</label>
		<input type="text" id="BoardRoll" name="BoardRoll"> <br>

		<label for="RegNo">Reg No</label>
		<input type="text" id="RegNo" name="RegNo"> <br>

		<label for="Session">Session</label>
		<input type="text" name="Session"> <br>

		<label for="Semester">Semester</label>
		<select name="Semester">
			<option value="">Select Semester</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
		</select> <br>

		<label for="Department">Department</label>
		<select name="Department">
			<option value="">Select Department</option>
			<option value="CMT">CMT</option>
			<option value="CT">CT</option>
			<option value="MT">MT</option>
			<option value="ENT">ENT</option>
			<option value="ET">ET</option>
			<option value="MRT">MRT</option>
			<option value="AIDT">AIDT</option>
		</select> <br>

		<label for="Photo">Photo</label>
		<input type="file" name="Photo"> <br>
 
		<input type="submit" name="submit" value="Store Information">
	</form>

	<table border="1px solid black">
		<thead>
			<tr>
				<th>Serial</th>
				<th>Name</th>
				<th>Class Roll</th>
				<th>Board Roll</th>
				<th>Reg No</th>
				<th>Session</th>
				<th>Semester</th>
				<th>Department</th>
				<th>Photo</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php while($Row = mysqli_fetch_array($Result,MYSQLI_ASSOC)){ ?>
				<tr>
					<td> <?php echo $Row['Serial']; ?> </td>
					<td> <?php echo $Row['Name']; ?> </td>
					<td> <?php echo $Row['ClassRoll']; ?> </td>
					<td> <?php echo $Row['BoardRoll']; ?> </td>
					<td> <?php echo $Row['RegNo']; ?> </td>
					<td> <?php echo $Row['Session']; ?> </td>
					<td> <?php echo $Row['Semester']; ?> </td>
					<td> <?php echo $Row['Department']; ?> </td>
					<td> 
						<img src="images/<?php echo $Row['Photo']; ?>" alt="Student Photo" height="50">
					</td>
					<td>
						<a href="edit.php?Action=Edit&Serial=<?php echo $Row['Serial']; ?>">Edit</a>
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>?Action=Delete&Serial=<?php echo $Row['Serial']; ?>">Delete</a>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

</body>
</html>