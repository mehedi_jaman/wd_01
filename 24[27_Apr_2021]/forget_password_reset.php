<?php 
	include('db.php');

	if (isset($_REQUEST['Email']) && isset($_REQUEST['Token'])) {
		$Email  = $_REQUEST['Email'];
		$Token  = $_REQUEST['Token'];

		$Query = "SELECT * FROM Users WHERE Email = '$Email' AND password_reset_token = '$Token'";

		$Result = mysqli_query($Connection,$Query);
		$Count = mysqli_num_rows($Result);
	}


	if (isset($_POST['submit'])) {
		if ($_POST['submit'] == 'Reset Password') {
			$Email = $_POST['Email'];
			$NewPassword = sha1($_POST['NewPassword']);
			$ConfirmNewPassword = sha1($_POST['ConfirmNewPassword']);

			if ($NewPassword != $ConfirmNewPassword) {
				echo "Error ! Password doesn't match";
			}
			else{
				$Query = "UPDATE Users SET Password = '$NewPassword', password_reset_token = '' WHERE Email = '$Email'";

				$Result = mysqli_query($Connection,$Query);

				if (!$Result) {
					echo "Password reeset error !";
				}
				else
					echo "Password reset successfull !";
			}
		}
	}
 ?>


<?php if ($Count > 0) { ?>
<!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 	<title>Password Reset</title>
 </head>
 <body>
 	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
 		<input type="hidden" name="Email" value="<?php echo $Email; ?>">
 		<label for="NewPassword">New Password</label>
 		<input type="password" name="NewPassword" id="NewPassword">

 		<label for="ConfirmNewPassword">Confirm New Password</label>
 		<input type="password" name="ConfirmNewPassword" id="ConfirmNewPassword">

 		<input type="submit" name="submit" value="Reset Password">
 	</form>
 </body>
 </html>	
<?php } else{
	echo "Password Reset Link Expired";
}
?>