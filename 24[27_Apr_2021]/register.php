<?php 

	include('db.php');

	if (isset($_POST['submit'])) {
		$Email 			 = $_POST['Email'];
		$Password 		 = sha1($_POST['Password']);
		$ConfirmPassword = sha1($_POST['ConfirmPassword']);
		$Image   		 = '';

		if (empty($Email) || empty($Password) || empty($ConfirmPassword)) {
			echo "Error ! You must input all fieds.";
		}
		else{
			if ($Password != $ConfirmPassword) {
				echo "Error ! Password doesn't match.";
			}
			else{
				$Query = "SELECT * FROM Users WHERE Email = '$Email'";

				$Result = mysqli_query($Connection, $Query);

				$Count = mysqli_num_rows($Result);

				if ($Count > 0) {
					echo "Sorry ! This email already exists !";
				}
				else
				{
					if (isset($_FILES['Image']['name'])) {
						if (!empty($_FILES['Image']['name'])) {

							$FileName = $_FILES['Image']['name'];
							$FileTmp  = $_FILES['Image']['tmp_name'];
							$FileSize = $_FILES['Image']['size'];

							// $Exploded = explode('.',$FileName);
							// $FileExt  = strtolower(end($Exploded));

							$FileExt  = pathinfo($FileName, PATHINFO_EXTENSION);

							if ($FileSize > 2097152) {
								echo "Sorry ! File size exceeds.";
							}

							if ($FileExt == 'jpg' || $FileExt == 'jpeg' || $FileExt == 'png') {
								$FileNewName = rand(9999,9999).rand(9999,99999).'.'.$FileExt;
								$Destination = 'images/'.$FileNewName;

								if (!move_uploaded_file($FileTmp, $Destination)) {
									echo "Something wrong ! File upload failed.";
								}
								else
									$Image = $FileNewName;
							}
							else
								echo "Sorry ! this is not an image. Please select a valid image.";
						}
					}


					$Query = "INSERT INTO Users (Email, Password, Image) VALUES('$Email','$Password','$Image')";

					$Result = mysqli_query($Connection, $Query);

					if (!$Result) {
						echo "Sorry ! Data insert failed";
					}
					else
						echo "Congratulations ! new user created successfully !";
				}
			}
		}
	}

	mysqli_close();

 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 	<title>Registration</title>
 </head>
 <body>
 	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="Email">Email</label>
		<input type="email" name="Email" required="required"> <br>

		<label for="Password">Password</label>
		<input type="password" name="Password" required="required"> <br>

		<label for="ConfirmPassword">Confirm Password</label>
		<input type="password" name="ConfirmPassword" required="required"> <br>

		<label for="Image">Image</label>
		<input type="file" name="Image"> <br>

		<input type="submit" name="submit" value="Register new user">
	</form>

	<a href="login.php">Login</a>
 </body>
 </html>