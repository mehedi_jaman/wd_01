<?php 
	session_start();
	include('db.php');


	if (isset($_POST['submit'])) {
		if ($_POST['submit'] == 'Reset Password') {
			$Email 				= $_SESSION['Email'];
			$CurrentPassword 	= sha1($_POST['CurrentPassword']);
			$NewPassword 		= sha1($_POST['NewPassword']);
			$ConfirmNewPassword = sha1($_POST['ConfirmNewPassword']);

			$Query = "SELECT * FROM Users WHERE Email = '$Email' AND Password = '$CurrentPassword'";

			$Result = mysqli_query($Connection,$Query);

			$Count = mysqli_num_rows($Result);

			if ($Count > 0) {
				if ($NewPassword != $ConfirmNewPassword) {
					$ErrorMsg = 'New password doesn\'t match';
				}
				else
				{
					$Query = "UPDATE Users SET Password = '$NewPassword' WHERE Email = '$Email'";
					$Result = mysqli_query($Connection,$Query);

					if (!$Result) {
						$ErrorMsg = 'Error in SQL Query';
					}
					else{
						$SuccessMsg = 'Password Reset Success !';
					}
				}
			}
			else
				$ErrorMsg = "Sorry your current Password Doesn't match !";
		}
	}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Password Reset</title>
</head>
<body>
	<?php if (isset($ErrorMsg)) { ?>
		<div style="text-align: center;color: red;"><?php echo $ErrorMsg; ?></div>
	<?php } ?>

	<?php if (isset($SuccessMsg)) { ?>
		<div style="text-align: center;color: green;"><?php echo $SuccessMsg; ?></div>
	<?php } ?>

	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="CurrentPassword">Current Password</label>
		<input type="password" name="CurrentPassword" id="CurrentPassword"> <br>

		<label for="NewPassword">New Password</label>
		<input type="password" name="NewPassword" id="NewPassword"> <br>

		<label for="ConfirmNewPassword">Confirm New Password</label>
		<input type="password" name="ConfirmNewPassword" id="ConfirmNewPassword"> <br>

		<input type="submit" name="submit" value="Reset Password">
	</form>

	<a href="index.php">Home</a>
</body>
</html>