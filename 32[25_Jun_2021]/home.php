
		<!-- Content Area -->
		<div class="row">
			<div class="col-md-9">
				<div class="col-md-12"> 

				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>মুজিব শতবর্ষ ক্ষণ গণনা</strong>
								</div>
								<div class="panel-body">
									<div class="col-md-4">
										<img src="images/mujib_logo.png" alt="Mujib Logo" class="img  img-responsive">
									</div>
									<div class="col-md-8">
										<div data-type="countdown" data-id="2599948" class="tickcounter" style="width: 100%; position: relative; padding-bottom: 25%">
											<a href="//www.tickcounter.com/countdown/2599948/" title="মুজিব বর্ষ ক্ষণ গণনা">মুজিব বর্ষ ক্ষণ গণনা</a><a href="//www.tickcounter.com/" title="Countdown">Countdown</a></div>
										<script>(function(d, s, id) { var js, pjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//www.tickcounter.com/static/js/loader.js"; pjs.parentNode.insertBefore(js, pjs); }(document, "script", "tickcounter-sdk"));</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Notice board -->
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>নোটিশ বোর্ড</strong>
								</div>
								<div class="panel-body">
									<ul>
										<li> <i class="fas fa-custom fa-check-circle"></i> <span class="label label-custom">18 Jun 2021</span> <a href="">উপবৃত্তি ফরম পূরনরে নির্দেশিকা</a></li>
										<li><i class="fas fa-custom fa-check-circle"></i> <span class="label label-custom">18 Jun 2021</span> <a href="">উপবৃত্তির ফরম পূরনের বিজ্ঞপ্তি (শুধুমাত্র ১ম পর্বের শিক্ষার্থীদের জন্য)</a></li>
										<li><i class="fas fa-custom fa-check-circle"></i> <span class="label label-custom">18 Jun 2021</span> <a href="">২য়,৪র্থ,৬ষ্ঠ ও ৮ম পর্বের ফরম ফিলাপের বিজ্ঞপ্তি</a></li>
										<li><i class="fas fa-custom fa-check-circle"></i> <span class="label label-custom">18 Jun 2021</span> <a href="">১ম ও ৩য় পর্বের পরিপূরক পরীক্ষা (২০১৯) এর ফলাফল</a></li>
										<li><i class="fas fa-custom fa-check-circle"></i> <span class="label label-custom">18 Jun 2021</span> <a href="">১ম পর্বের টেকনোলজি ভিত্তিক অনলাইন ক্লাশ রুটিন-২০২১</a></li>
									</ul>
								</div>
								<div class="panel-footer">
									<div class="pull-right"><a href="" class="btn btn-custom">সকল নোটিশ</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/Directorate.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  লক্ষ্য ও উদ্দেশ্য</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  কর্মবন্টন</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   সিটিজেন চার্টার</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/training.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রতিষ্ঠানের তালিকা</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রশিক্ষণ আদেশ-বিদেশে</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   প্রশিক্ষণ আদেশ-দেশে</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/Directorate.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  লক্ষ্য ও উদ্দেশ্য</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  কর্মবন্টন</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   সিটিজেন চার্টার</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/training.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রতিষ্ঠানের তালিকা</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রশিক্ষণ আদেশ-বিদেশে</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   প্রশিক্ষণ আদেশ-দেশে</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/Directorate.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  লক্ষ্য ও উদ্দেশ্য</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  কর্মবন্টন</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   সিটিজেন চার্টার</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/training.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রতিষ্ঠানের তালিকা</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রশিক্ষণ আদেশ-বিদেশে</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   প্রশিক্ষণ আদেশ-দেশে</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/Directorate.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  লক্ষ্য ও উদ্দেশ্য</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  কর্মবন্টন</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   সিটিজেন চার্টার</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/training.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রতিষ্ঠানের তালিকা</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রশিক্ষণ আদেশ-বিদেশে</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   প্রশিক্ষণ আদেশ-দেশে</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/Directorate.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  লক্ষ্য ও উদ্দেশ্য</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  কর্মবন্টন</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   সিটিজেন চার্টার</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/training.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রতিষ্ঠানের তালিকা</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রশিক্ষণ আদেশ-বিদেশে</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   প্রশিক্ষণ আদেশ-দেশে</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/Directorate.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  লক্ষ্য ও উদ্দেশ্য</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  কর্মবন্টন</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   সিটিজেন চার্টার</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="panel panel-custom">
										<div class="panel-heading text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
										<div class="panel-body">
											<div class="col-md-4">
												<img src="images/training.jpg" alt="" class="img img-responsive">
											</div>
											<div class="col-md-8">
												<ul>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রতিষ্ঠানের তালিকা</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>  প্রশিক্ষণ আদেশ-বিদেশে</a></li>
													<li><a href=""><i class="fas fa-custom fa-check-circle"></i>   প্রশিক্ষণ আদেশ-দেশে</a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>কারিগরি শিক্ষা</strong>
								</div>
								<div class="panel-body">
									<iframe class="ytcontent" width="100%" height="400" src="https://www.youtube.com/embed/3qJp_RfEztk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>গুগল ম্যাপ</strong>
								</div>
								<div class="panel-body">
									<iframe class="ytcontent" width="100%" height="400" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=52.70967533219885, -8.020019531250002&amp;q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br />
								</div>
							</div>
						</div>
					</div>
				</div> 
				</div>
			</div>