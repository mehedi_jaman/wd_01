<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Register</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">WEB</a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="/">Home</a></li>
	        <li><a href="login.php">Login</a></li>	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="row">
		<div class="container">
			<div class="col-md-6 col-md-offset-3">
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-horizontal">
					<div class="form-group">
						<label for="Email" class="control-label col-md-4">Email</label>
						<div class="col-md-8">
							<input type="Email" name="Email" id="Email" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label for="Password" class="control-label col-md-4">Password</label>
						<div class="col-md-8">
							<input type="password" name="Password" id="Password" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label for="ConfirmPassword" class="control-label col-md-4">Confirm Password</label>
						<div class="col-md-8">
							<input type="password" name="ConfirmPassword" id="ConfirmPassword" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<label for="Image" class="control-label col-md-4">Image</label>
						<div class="col-md-8">
							<input type="file" name="Image" id="Image" class="form-control">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-8 col-md-offset-4">
							<input type="submit" name="submit" value="Register" class="btn btn-primary btn-block">

							Already have an account? <a href="login.php">Login</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>