
		<!-- Content Area -->
		<div class="row">
			<div class="col-md-9">
				<div class="col-md-12">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>লক্ষ্য (রুপকল্প)</strong>
								</div>
								<div class="panel-body">
									<p>
										“বাংলাদেশের প্রযুক্তিগত এবং আর্থ সামাজিক উন্নয়নে অবদান রাখার জন্য ব্যাপকভাবে শিল্পের সঙ্গে মান সম্মত শিক্ষা ও প্রশিক্ষণ, সঠিক নির্দেশনা এবং পরামর্শ, সহযোগিতা প্রদানের মাধ্যমে  খুলনা পলিটেকনিক ইন্সটিটিউটকে স্বনির্ভরশীল প্রমাণ করা।”
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>