<?php 
  
  $Host = 'localhost';
  $User = 'phpmyadmin';
  $Password = 'Q1w2e3r4t5!';
  $DB   = 'kpi';

  $Connection = mysqli_connect($Host,$User,$Password,$DB);

  if (isset($_REQUEST['submit'])) {
    if ($_REQUEST['submit'] == 'submit') {
      $Name       = $_REQUEST['name'];
      $Technology = $_REQUEST['technology'];
      $Semester   = $_REQUEST['semester'];
      $Shift      = $_REQUEST['shift'];
      $Session    = $_REQUEST['session'];
      $Gender     = $_REQUEST['gender'];

     $Query = "INSERT INTO students (
        name,
        technology_id,
        semester,
        shift,
        session,
        gender
      ) VALUES(
        '$Name',
        $Technology,
        '$Semester',
        '$Shift',
        '$Session',
        '$Gender'
      )";

      $Result = mysqli_query($Connection, $Query);

      if (!$Result) {
        $ErrorMsg = 'Data Insertion Error !';
      }
      else
        $SuccessMsg = 'Data Insertion Successfull';
    }
  }

  $Query = "SELECT * FROM students";
  $Students = mysqli_query($Connection,$Query);


 ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Students</h1>

          <div class="row">
            <?php 
            if (isset($SuccessMsg)) {
              echo '<div class="label label-success">'.$SuccessMsg.'</div>';
            }


            if (isset($ErrorMsg)) {
              echo '<div class="label label-warning">'.$ErrorMsg.'</div>';
            }

           ?>
          </div>

          <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#student_add_modal">Add New Student</button>
          <table class="table table-responsive table-bordered">
            <thead>
              <tr>
                <th>Serial</th>
                <th>Name</th>
                <th>Technology</th>
                <th>Semester</th>
                <th>Shift</th>
                <th>Session</th>
                <th>Gender</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php while($Student = mysqli_fetch_assoc($Students)){?>
              <tr>
                <td><?php echo $Student['id']; ?></td>
                <td><?php echo $Student['name']; ?></td>
                <td><?php echo $Student['technology_id']; ?></td>
                <td><?php echo $Student['semester']; ?></td>
                <td><?php echo $Student['shift']; ?></td>
                <td><?php echo $Student['session']; ?></td>
                <td><?php echo $Student['gender']; ?></td>
                <td>
                  <a href="" class="btn btn-warning">Edit</a>
                  <a href="" class="btn btn-danger">Delete</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>

          <div class="modal fade" id="student_add_modal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="gridSystemModalLabel">New Student</h4>
                </div>
                <div class="modal-body">
                  <form action="index.php?page=student" method="POST" enctype="multipart/form-data" class="form form-horizontal">
                    <div class="form-group">
                      <label for="name" class="control-label col-md-2">Name</label>
                      <div class="col-md-10">
                        <input type="text" name="name" class="form-control">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="technology" class="control-label col-md-2">Technology</label>
                      <div class="col-md-10">
                        <input type="text" name="technology" class="form-control">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="semester" class="control-label col-md-2">Semester</label>
                      <div class="col-md-10">
                        <select name="semester" id="semester" class="form-control">
                          <option value="">Select Semester</option>
                          <?php for($i = 1; $i <= 8; $i++){?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?> Semester</option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="shift" class="control-label col-md-2">Shift</label>
                      <div class="col-md-10">
                        <select name="shift" class="form-control" id="">
                          <option value="">Select Shift</option>
                          <option value="First Shift">First Shift</option>
                          <option value="Second Shift">Second Shift</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="session" class="control-label col-md-2">Session</label>
                      <div class="col-md-10">
                        <select name="session" id="" class="form-control">
                        <?php 
                          $Year = date('Y');
                          $Limit = $Year - 5;

                          for($Year; $Year > $Limit; $Year--){
                            $PreviousYear = $Year -1;
                         ?>
                         <option value="<?php echo $PreviousYear.'-'.$Year ?>"><?php echo $PreviousYear.'-'.$Year ?></option>

                       <?php } ?>
                       </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="gender" class="control-label col-md-2">Gender</label>
                      <div class="col-md-10">
                        <select name="gender" id="" class="form-control">
                          <option value="">Select Gender</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                          <option value="Other">Other</option>
                        </select>
                      </div>
                    </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" name="submit" value="submit" class="btn btn-primary">Save</button>
                  </form>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
        </div>
      </div>
    </div>
