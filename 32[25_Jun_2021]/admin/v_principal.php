
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Vice Principal</h1>

       
          <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <img src="" alt="Vice Principal Image" class="img img-responsive img-rounded img-thumbnail">
            </div>
          </div>
          
          <div class="panel panel-primary">
            <div class="panel-heading">Vice Principal</div>
            <div class="panel-body">
              <form action="" class="form-horizontal">
                <div class="form-group">
                  <label for="name" class="control-label col-md-2">Name</label>
                  <div class="col-md-10">
                    <input type="text" name="name" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="message" class="control-label col-md-2">Message</label>
                  <div class="col-md-10">
                    <textarea name="message" id="mesasge" class="form-control" cols="30" rows="10"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="photo" class="control-label col-md-2">Photo</label>
                  <div class="col-md-10">
                    <input type="file" name="photo" class="form-control">
                  </div>
                </div>

                <div class="col-md-2 col-md-offset-4">
                  <input type="submit" name="submit" value="Update" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
