<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Bootstrap</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
	<link rel="stylesheet" href="libs/fontawesome/css/fontawesome.min.css">
	<link rel="stylesheet" href="libs/fontawesome/css/brands.min.css">
	<link rel="stylesheet" href="libs/fontawesome/css/solid.min.css">
	<link rel="stylesheet" href="libs/fontawesome/css/regular.min.css">
	<style>
		.navbar-blue{
			background-color: blue;
			color: white;
		}
		.navbar-pepe{
			background-color: green;
			color: white;
		}
		.navbar-sky{
			background-color: black;
			color: white;
		}
		a{
			color: white;
		}

		.navbar-secondary{
			margin-bottom: 0px;
			height: 30px;
			padding-top: 2px;
		}

		.navbar{
			min-height: 0px;
		}

		.secondary-nav{
			/*padding-top: 5px;*/
			/*margin-top: -5px;*/
		}

		.text-center{
			text-align: center;
		}

		.ytcontent{
			width: 100%;
			height: 400px;
		}
		.ytcontainer{
			padding: 0px;
		}
	</style>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-sky navbar-secondary">
			<!-- <div class="container-fluid"> -->
				<div class="col-md-6">
					Phone : +8801914 090747, Email : sujonskynet@gmail.com
				</div>
				<div class="col-md-2">
					<a href="#"><i class="fab fa-facebook-f  "></i></a>
					<a href="#"><i class="fab fa-twitter "></i></a>
					<a href="#"><i class="fab fa-youtube "></i></a>
				</div>		
			<!-- </div>		 -->
		</nav>
		<nav class="navbar navbar-pepe">
			<!-- <div class="container-fluid"> -->
				<div class="navbar-header">
					<a href="#" class="navbar-brand">CPIK</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="">Home</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Service</a>

							<ul class="dropdown-menu">
								<li><a href="">Web Development</a></li>
								<li><a href="">SMM</a></li>
								<li><a href="">SEO</a></li>
								<li><a href="">Content Writing</a></li>
							</ul>
						</li>
						<li><a href="">About</a></li>
						<li><a href="">Contact</a></li>
					</ul>
				</div>
			<!-- </div>		 -->
		</nav>

		<div class="row">
			<div class="col-md-9">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center">মুজিব শতবর্ষ ক্ষণ গণনা</div>
							</div>
							<div class="panel-body">
								Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Porro assumenda repellendus eveniet doloribus facere velit. Nisi veniam ad veritatis excepturi deserunt. Nulla, est? Animi, corrupti praesentium excepturi odit, minima doloremque.
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio suscipit nam labore nesciunt explicabo necessitatibus eos dolores, iste temporibus, odit rem ipsum error voluptate odio consectetur. Perferendis a nobis, magnam.
								Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Placeat quo, repellendus odit iste sit incidunt quidem atque obcaecati accusantium culpa harum perferendis est assumenda numquam, dolores nisi ab vitae commodi?
							</div>					
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>নোটিশ বোর্ড</strong></div>
							</div>
							<div class="panel-body">
								Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos laborum velit, fugiat porro atque, maiores enim sed omnis placeat ducimus suscipit debitis labore pariatur quaerat libero, minima, sequi aliquid fugit.
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere officia assumenda obcaecati laboriosam. Recusandae, tempora corrupti enim perspiciatis consectetur id eligendi sunt delectus distinctio. Sapiente repellat exercitationem ipsum pariatur necessitatibus!
								 Lorem ipsum dolor sit, amet consectetur adipisicing, elit. Sit earum, accusantium adipisci dolore fugiat cum quos, sed molestiae tenetur modi dicta saepe, aliquam praesentium optio ut ea voluptas assumenda hic.
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>ইন্সটিটিউট সম্পর্কিত</strong></div>
							</div>
							<div class="panel-body">
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore quibusdam, consequatur dolore, rem soluta voluptatum! Porro corporis sint eius, nihil dolore tempora quis. Fugit ab incidunt, pariatur libero magnam delectus.
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>প্রশিক্ষণ সংক্রান্ত</strong></div>
							</div>
							<div class="panel-body">
								Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Tempore ipsum provident, eaque, corrupti reiciendis voluptas officiis totam commodi ullam dolores delectus quisquam vel dolor animi magnam ea, sint illum quidem?
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>কারিগরি শিক্ষা</strong></div>
							</div>
							<div class="panel-body ytcontainer">
								<iframe class="ytcontent" width="560" height="315" src="https://www.youtube.com/embed/3qJp_RfEztk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>Location</strong></div>
							</div>
							<div class="panel-body ytcontainer">
								<iframe class="ytcontent" width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=52.70967533219885, -8.020019531250002&amp;q=1%20Grafton%20Street%2C%20Dublin%2C%20Ireland&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br />

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>অধ্যক্ষ</strong></div>
							</div>
							<div class="panel-body">
								<img src="" alt="" class="img img-responsive">
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>জাতীয় সংগীত</strong></div>
							</div>
							<div class="panel-body">
								<audio src="" type="audio/ogg"></audio>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="text-center"><strong>ভিজিটর কাউন্টার</strong></div>
							</div>
							<div class="panel-body">
								<div id="sfc14ettr8fnstzczgn4wy4gb31q9g8xmjd"></div>
<script type="text/javascript" src="https://counter6.stat.ovh/private/counter.js?c=14ettr8fnstzczgn4wy4gb31q9g8xmjd&down=async" async></script>
<noscript><a href="https://www.freecounterstat.com" title="free hit counter"><img src="https://counter6.stat.ovh/private/freecounterstat.php?c=14ettr8fnstzczgn4wy4gb31q9g8xmjd" border="0" title="free hit counter" alt="free hit counter"></a></noscript>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>