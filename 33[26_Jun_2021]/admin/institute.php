
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Institute</h1>

       
          <div class="panel panel-primary">
            <div class="panel-heading">
              Institute
            </div>
            <div class="panel-body">
              <form action="" class="form form-horizontal">
                <div class="form-group">
                  <label for="name" class="control-label col-md-2">Name</label>
                  <div class="col-md-4">
                    <input type="text" name="name" class="form-control">
                  </div>

                  <label for="logo" class="control-label col-md-2">Logo</label>
                  <div class="col-md-4">
                    <input type="file" name="logo" class="form-control">
                  </div>
                </div>              

                <div class="form-group">
                  <label for="phone" class="control-label col-md-2">Phone</label>
                  <div class="col-md-4">
                    <input type="text" name="phone" class="form-control">
                  </div>

                  <label for="email" class="control-label col-md-2">Email</label>
                  <div class="col-md-4">
                    <input type="email" name="email" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="fax" class="control-label col-md-2">Fax</label>
                  <div class="col-md-4">
                    <input type="text" name="fax" class="form-control">
                  </div>

                  <label for="google_map_location" class="control-label col-md-2">Google Map</label>
                  <div class="col-md-4">
                    <input type="text" name="google_map_location" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="address" class="control-label col-md-2">Address</label>
                  <div class="col-md-10">
                    <input type="text" name="address" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="about" class="control-label col-md-2">About</label>
                  <div class="col-md-10">
                    <textarea name="about" id="about" class="form-control" cols="30" rows="10"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="mission" class="control-label col-md-2">Mission</label>
                  <div class="col-md-10">
                    <textarea name="mission" id="mission" class="form-control" cols="30" rows="10"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="vision" class="control-label col-md-2">Vision</label>
                  <div class="col-md-10">
                    <textarea name="vision" id="vision" class="form-control" cols="30" rows="10"></textarea>
                  </div>
                </div>

                <div class="col-md-2 col-md-offset-5">
                  <input type="submit" name="submit" value="Update" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
