<?php 
  
  include('db.php');

  if(isset($_REQUEST['submit'])){
    if ($_REQUEST['submit'] == 'Add Technology') {
      $Name = $_REQUEST['name'];

      $Query = "INSERT INTO technologies (name) VALUES('$Name')";

      $Result = mysqli_query($Connection,$Query);

      if (!$Result) {
        $ErrorMsg = 'Data insertion error !';
      }
      else
        $SuccessMsg = 'Successfull !';
    }
  }

  $Query = "SELECT * FROM technologies";
  $Technologies  = mysqli_query($Connection,$Query);

 ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Technology</h1>

          <div class="row">
            <?php 
            if (isset($SuccessMsg)) {
              echo '<div class="label label-success">'.$SuccessMsg.'</div>';
            }


            if (isset($ErrorMsg)) {
              echo '<div class="label label-warning">'.$ErrorMsg.'</div>';
            }

           ?>
          </div>
       
          <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#NewTechnologyModal">Add New Technology</button>
          <table class="table table-responsive table-bordered">
            <thead>
              <tr>
                <th>Serial</th>
                <th>Name</th>                
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php while($Technology = mysqli_fetch_assoc($Technologies)) {?>
              <tr>
                <td><?php echo $Technology['id']; ?></td>
                <td><?php echo $Technology['name']; ?></td>
                <td>
                  <a href="" class="btn btn-warning">Edit</a>
                  <a href="" class="btn btn-danger">Delete</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>

          <div class="modal fade" id="NewTechnologyModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add New Technology</h4>
              </div>
              <div class="modal-body">
                <form action="index.php?page=technology" method="post" enctype="multipart/form-data" class="form form-horizontal">
                  <div class="form-group">
                    <label for="name" class="control-label col-md-2">Name</label>
                    <div class="col-md-10">
                      <input type="text" name="name" class="form-control" placeholder="i.e. Computer Technology">
                    </div>
                  </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" name="submit" value="Add Technology" class="btn btn-primary">
                </form>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        </div>
      </div>
    </div>
