
			<div class="col-md-3"> 
				<div class="col-md-12">

				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>অধ্যক্ষ</strong>
								</div>
								<div class="panel-body principal-panel" style="overflow: hidden;">
									<img src="images/principal.png" alt="" class="img img-responsive">
								</div>
								<div class="panel-footer text-center">
									<h3>প্রকৌশলী অনিমেশ পাল</h3>
									<a href="">বিস্তারিত</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<img src="images/slogan.png" alt="" class="img img-responsive">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center">
									<strong>উপাধ্যক্ষ</strong>
								</div>
								<div class="panel-body">
									<img src="images/v_principal.png" alt="" class="img img-responsive">
								</div>
								<div class="panel-footer text-center">
									<h3>গাজী নূরুল ঈমান সামছি</h3>
									<a href="">বিস্তারিত</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center"><strong>শিক্ষা বোর্ডের ফলাফল</strong></div>
								<div class="panel-body">
									<ul>
										<li><i class="fas fa-check-circle fa-custom"></i> <a href="http://www.educationboardresults.gov.bd/" target="_blank"> শিক্ষাবোর্ডের ফলাফল-টেলিটক</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href=""> শিক্ষাবোর্ডের ফলাফল-ওয়েব</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href="">জাতীয় বিশ্ববিদ্যালয়ের ফলাফল</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href=""> প্রাইমারি সমাপনী পরীক্ষার ফলাফল</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href=""> উন্মুক্ত বিশ্ববিদ্যালয়ের ফলাফল</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href="">এনটিআরসিএ পরীক্ষার ফলাফল</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center"><strong>গুরুত্বপূর্ণ লিংক</strong></div>
								<div class="panel-body">
									<ul>
										<li><i class="fas fa-check-circle fa-custom"></i> <a href="http://www.educationboardresults.gov.bd/" target="_blank"> শিক্ষাবোর্ডের ফলাফল-টেলিটক</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href=""> শিক্ষাবোর্ডের ফলাফল-ওয়েব</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href="">জাতীয় বিশ্ববিদ্যালয়ের ফলাফল</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href=""> প্রাইমারি সমাপনী পরীক্ষার ফলাফল</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href=""> উন্মুক্ত বিশ্ববিদ্যালয়ের ফলাফল</a></li>
										<li><i class="fas fa-check-circle fa-custom"></i><a href="">এনটিআরসিএ পরীক্ষার ফলাফল</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center"><strong>জরুরী হটলাইন</strong></div>
								<div class="panel-body">
									<img src="images/Hotline_BN.png" alt="" class="img img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center"><strong>ডেঙ্গু প্রতিরোধে করণীয়</strong></div>
								<div class="panel-body">
									<a href="">
										<img src="images/dengu.jpg" alt="" class="img img-responsive">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<a href="">
								<img src="images/student-corner.png" alt="" class="img img-responsive">
							</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center"><strong>জাতীয় সংগীত</strong></div>
								<div class="panel-body">
									<audio class="audio" src="media/tamanna.mp3" controls="true"></audio>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="panel panel-custom">
						<div class="panel-body">
							<div class="panel panel-custom no-border">
								<div class="panel-heading text-center"><strong>ভিজিটর কাউন্টার</strong></div>
								<div class="panel-body">
									<a href='http://besucherzaehler.co'>Besucherstatistik</a>
									<script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=12020f34eddcc02a1c4e01cb9506398fe350ddef'></script>
									<script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/839550/t/9"></script>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>

		