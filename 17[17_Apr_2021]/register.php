<?php 
	include('db.php');

	if (isset($_POST['submit'])) {
		$Email 			 = $_POST['Email'];
		$Password 		 = md5($_POST['Password']);
		$ConfirmPassword = md5($_POST['ConfirmPassword']);
		$Image 			 = '';

		if ($Password != $ConfirmPassword) {
			echo "Password doesn't match";
		}
		else{
			$Query  = "SELECT * FROM Users WHERE Email = '$Email'";
			$Result = mysqli_query($Connection, $Query);
			$Count  = mysqli_num_rows($Result);

			if ($Count > 0) {
				echo "This Email is already registered !";
			}
			else{
				if (isset($_FILES['Image']['name'])) {
					if (!empty($_FILES['Image']['name'])) {
						$FileName = $_FILES['Image']['name'];
						$FileSize = $_FILES['Image']['size'];
						$FileType = $_FILES['Image']['type'];
						$FileTmp  = $_FILES['Image']['tmp_name'];
						$FileExt  = strtolower(end(explode('.',$FileName)));

						if ($FileSize > 500000) {
							echo "File size exceeds ! Photo Not uploaded.";
						}

						if ($FileExt == 'jpg' || $FileExt == 'jpeg' || $FileExt == 'bmp' || $FileExt == 'png') {
							
							$FileNewName = rand(9999,99999).'.'.$FileExt;
							$TargetDir = 'images/'.$FileNewName;

							if (!move_uploaded_file($FileTmp, $TargetDir)) {
								echo "Something Wrong ! Image upload failed.";
							}
							else
								$Image = $FileNewName;
						}
						else
							echo "This is not a valid image. Please select an Image";
					}
				}


				$Query = "INSERT INTO Users (Email,Password,Image) VALUES('$Email','$Password','$Image')";

				$Result = mysqli_query($Connection,$Query);

				if (!$Result) {
					echo "Query Execution failed !";
				}
				else
					echo "New user created Successfully !";
			}
		}
	}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>User Registration</title>
</head>
<body>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="Email">Email</label>
		<input type="email" name="Email" required="required"> <br>

		<label for="Password">Password</label>
		<input type="password" name="Password" required="required"> <br>

		<label for="ConfirmPassword">Confirm Password</label>
		<input type="password" name="ConfirmPassword" required="required"> <br>

		<label for="Image">Image</label>
		<input type="file" name="Image"> <br>

		<input type="submit" name="submit" value="Register new user">
	</form>
</body>
</html>