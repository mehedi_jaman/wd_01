<?php 
	session_start();

	if ($_SESSION['Status'] != 1) {
		header('location:login.php');	
	}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>List</title>
</head>
<body>
	<h1>This is List Page</h1>
	<p>
		Lorem, ipsum dolor sit amet consectetur, adipisicing elit. Explicabo officiis odio doloribus odit distinctio magni architecto cumque a culpa tempore, nulla nesciunt illum perferendis quae blanditiis quia. Ab, quos, sunt.
	</p>

	<a href="index.php">Home</a>
	<a href="gallery.php">Gallery</a>
	<a href="logout.php">Logout</a>
</body>
</html>