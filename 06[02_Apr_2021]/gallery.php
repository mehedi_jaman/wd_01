<?php 
	session_start();

	if ($_SESSION['Status'] != 1) {
		header('location:login.php');	
	}

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Gallery</title>
</head>
<body>
	<h1>This is Gallery Page</h1>

	<p>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam accusamus autem aut, sequi maiores repudiandae? Suscipit amet accusantium ipsam similique beatae eos, praesentium doloribus eius porro quam, corrupti perspiciatis ducimus.
	</p>

	<a href="index.php">Home</a>
	<a href="list.php">List</a>
	<a href="logout.php">Logout</a>
</body>
</html>