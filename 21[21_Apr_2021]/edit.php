<?php 
	session_start();

	include('db.php');
	
	if ($_SESSION['Status'] != 1) {
		header('location:logout.php');
	}

	if (isset($_REQUEST['Action'])) {
		if ($_REQUEST['Action'] == 'Edit') {
			$Serial = $_REQUEST['Serial'];

			$Query = "SELECT * FROM StudentList WHERE `Serial` = '$Serial'";
			$Result = mysqli_query($Connection,$Query);

			$Data = mysqli_fetch_assoc($Result);

			$Name 		= $Data['Name'];
			$ClassRoll  = $Data['ClassRoll'];
			$BoardRoll  = $Data['BoardRoll'];
			$RegNo  	= $Data['RegNo'];
			$Session  	= $Data['Session'];
			$Semester  	= $Data['Semester'];
			$Department = $Data['Department'];
			$Photo  	= $Data['Photo'];
		}
	}


	if (isset($_POST['submit'])) {
		if ($_POST['submit'] == 'Update Information') {
			$Name 		= $_POST['Name'];
			$ClassRoll 	= $_POST['ClassRoll'];
			$BoardRoll 	= $_POST['BoardRoll'];
			$RegNo 		= $_POST['RegNo'];
			$Session 	= $_POST['Session'];
			$Semester 	= $_POST['Semester'];
			$Department = $_POST['Department'];

			$Query = "UPDATE StudentList SET Name = '$Name',ClassRoll = '$ClassRoll',BoardRoll='$BoardRoll',RegNo = '$RegNo',Session = '$ession', Semester = '$Semester',Department = '$Department'";

			$Result = mysqli_query($Connection,$Query);

			if (!$Result) {
				echo "Update failed";
			}
			else
			{
				header('location:insert.php?Success=Updateed+Successfully !');
			}
		}
	}

	mysqli_close();
	
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Update Student</title>
</head>
<body>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
		<label for="Name">Name</label>
		<input type="text" name="Name" value="<?php echo $Name; ?>"> <br>

		<label for="ClassRoll">Class Roll</label>
		<input type="text" name="ClassRoll" value="<?php echo $ClassRoll ?>"> <br>

		<label for="BoardRoll">Board Roll</label>
		<input type="text" name="BoardRoll" value="<?php echo $BoardRoll ?>"> <br>

		<label for="RegNo">Reg No</label>
		<input type="text" name="RegNo" value="<?php echo $RegNo ?>"> <br>

		<label for="Session">Session</label>
		<input type="text" name="Session" value="<?php echo $Session ?>"> <br>

		<label for="Semester">Semester</label>
		<input type="text" name="Semester" value="<?php echo $Semester ?>"> <br>
		<!-- <select name="Semester" value="<?php echo $Semester ?>">
			<option value="">Select Semester</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
		</select> <br> -->

		<label for="Department">Department</label>
		<input type="text" name="Department" value="<?php echo $Department ?>"> <br>
		<!-- <select name="Department" value="<?php echo $Department ?>">
			<option value="">Select Department</option>
			<option value="CMT">CMT</option>
			<option value="CT">CT</option>
			<option value="MT">MT</option>
			<option value="ENT">ENT</option>
			<option value="ET">ET</option>
			<option value="MRT">MRT</option>
			<option value="AIDT">AIDT</option>
		</select> <br> -->

		<!-- <label for="Photo">Photo</label>
		<input type="file" name="Photo"> <br> -->
 
		<input type="submit" name="submit" value="Update Information">
	</form>
</body>
</html>