<?php 
	session_start();

	if ($_SESSION['Status'] != 1) {
		header('location:login.php');
	}
 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web Development Class</title>
</head>
<body>
	<h1>Web Development Class</h1>
	<p>
		Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe explicabo fugiat rem perferendis perspiciatis nihil molestias laudantium natus voluptas, labore. Quibusdam beatae ullam, vel quaerat temporibus. Laboriosam, suscipit voluptatibus nemo.
	</p>

	<a href="logout.php">Logout</a>
</body>
</html>