<?php 
	session_start();

	if ($_SESSION['Status'] != 1) {
		header('location:login.php');
	}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Web Dev Class</title>
</head>
<body>
	<h1>This is Index Page</h1>

	<p>
		Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Molestias id veritatis voluptatum nemo ab ullam ipsam! Laboriosam, magni sequi, ab veniam temporibus harum, deleniti dolores maxime aspernatur similique facilis dolor.
	</p>

	<p>
		Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum a corporis, eligendi non cupiditate repellat eius tenetur optio culpa eaque saepe exercitationem soluta mollitia provident ab fugit voluptatibus iusto incidunt?
	</p>

	<p>
		Lorem ipsum dolor sit amet, consectetur, adipisicing elit. Ut reprehenderit fugiat iste reiciendis minima quae quam labore quos quas tempora, explicabo molestias? Perferendis nam, aut necessitatibus fugiat modi itaque, ut!
	</p>

	<a href="gallery.php">Gallery</a>
	<a href="list.php">List</a>
	<a href="logout.php">Logout</a>
</body>
</html>