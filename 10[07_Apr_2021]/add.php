<?php 
	
	$ServerName = 'localhost';
	$UserName	= 'phpmyadmin';
	$Password 	= 'Q1w2e3r4t5!';
	$DBName		= 'web_dev';

	$Connection = mysqli_connect($ServerName,$UserName,$Password,$DBName);

	if (!$Connection) {
		die('Connection Error'.mysqli_connect_error());
	}


	if (isset($_POST['submit'])) {
		$Name 		= $_POST['Name'];
		$ClassRoll 	= $_POST['ClassRoll'];
		$BoardRoll 	= $_POST['BoardRoll'];
		$RegNo 		= $_POST['RegNo'];
		$Session 	= $_POST['Session'];
		$Semester 	= $_POST['Semester'];
		$Department = $_POST['Department'];

		$Query 		= "INSERT INTO StudentList (Name,ClassRoll,BoardRoll,RegNo,Session,Semester,Department) VALUES('$Name','$ClassRoll','$BoardRoll','$RegNo','$Session',$Semester,'$Department')";

		$Result = mysqli_query($Connection,$Query);

		if ($Result) {
			echo "New Student Added Successfully !";
		}
		else
			echo "Error Adding Student !";
	}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>User Registration</title>
</head>
<body>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
		<label for="Name">Name</label>
		<input type="text" name="Name"> <br>

		<label for="ClassRoll">ClassRoll</label>
		<input type="text" name="ClassRoll"> <br>

		<label for="BoardRoll">BoardRoll</label>
		<input type="text" name="BoardRoll"> <br>

		<label for="RegNo">RegNo</label>
		<input type="text" name="RegNo"> <br>

		<label for="Session">Session</label>
		<input type="text" name="Session"> <br>

		<label for="Semester">Semester</label>
		<select name="Semester">
			<option value="">Select Semester</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
		</select> <br>

		<label for="Department">Department</label>
		<input type="text" name="Department"> <br>

		<input type="submit" name="submit" value="Add Student">
	</form>
</body>
</html>