<?php 
  include('db.php');


  if (isset($_REQUEST['submit']) && $_REQUEST['submit'] == 'Update') {
    $Name = $_REQUEST['name'];
    $Message = $_REQUEST['message'];

    $Query = "UPDATE principal SET name = '$Name', message = '$Message'";

    $Result = mysqli_query($Connection,$Query);

    if (!$Result) 
      $ErrorMsg = 'Update error !';    
    else
      $SuccessMsg = "Update Successfull !";
  }


  $Query = "SELECT * FROM principal";

  $Result = mysqli_query($Connection,$Query);
  $Result = mysqli_fetch_assoc($Result);

 ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Principal</h1>
          <div class="row">
            <?php 
            if (isset($SuccessMsg)) {
              echo '<div class="label label-success">'.$SuccessMsg.'</div>';
            }


            if (isset($ErrorMsg)) {
              echo '<div class="label label-warning">'.$ErrorMsg.'</div>';
            }

           ?>
          </div>

          <div class="row">
            <div class="col-md-4 col-md-offset-4">
              <img src="../images/<?php echo $Result['photo'];  ?>" alt="Principal Image" class="img img-responsive img-rounded img-thumbnail">
            </div>
          </div>
          
          <div class="panel panel-primary">
            <div class="panel-heading">Principal</div>
            <div class="panel-body">
              <form action="index.php?page=principal" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="form-group">
                  <label for="name" class="control-label col-md-2">Name</label>
                  <div class="col-md-10">
                    <input type="text" name="name" class="form-control" value="<?php echo $Result['name']; ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="message" class="control-label col-md-2">Message</label>
                  <div class="col-md-10">
                    <textarea name="message" id="mesasge" class="form-control" cols="30" rows="10" ><?php echo $Result['message']; ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="photo" class="control-label col-md-2">Photo</label>
                  <div class="col-md-10">
                    <input type="file" name="photo" class="form-control">
                  </div>
                </div>

                <div class="col-md-2 col-md-offset-4">
                  <input type="submit" name="submit" value="Update" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
