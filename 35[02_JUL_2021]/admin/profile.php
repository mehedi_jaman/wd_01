
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Profile</h1>
          <div class="panel panel-primary">
            <div class="panel-heading">My Profile</div>
            <div class="panel-body">
              <form action="" class="form form-horizontal">
                <div class="form-group">
                  <label for="name" class="control-label col-md-2">Name</label>
                  <div class="col-md-10">
                    <input type="text" name="name" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="email" class="control-label col-md-2">Email</label>
                  <div class="col-md-10">
                    <input type="text" name="email" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="password" class="control-label col-md-2">Password</label>
                  <div class="col-md-10">
                    <input type="text" name="password" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label for="confirm_password" class="control-label col-md-2">Confirm Password</label>
                  <div class="col-md-10">
                    <input type="text" name="confirm_password" class="form-control">
                  </div>
                </div>

                <div class="col-md-2 col-md-offset-5">
                  <input type="submit" name="submit" value="Update Profile" class="btn btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
