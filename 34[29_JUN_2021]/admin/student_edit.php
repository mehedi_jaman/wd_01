<?php 
  include('db.php');

  $ID = $_REQUEST['id'];
  $Query = "SELECT * FROM students WHERE id = ".$ID;
  $Student = mysqli_query($Connection,$Query);
  $Student = mysqli_fetch_assoc($Student);


  if(isset($_REQUEST['submit'])){
    if ($_REQUEST['submit'] == 'Update Student' ) {
      $ID           = $_REQUEST['id'];
      $Name         = $_REQUEST['name'];
      $TechnologyID = $_REQUEST['technology_id'];
      $Session      = $_REQUEST['session'];
      $Semester     = $_REQUEST['semester'];
      $Shift        = $_REQUEST['shift'];
      $Gender       = $_REQUEST['gender'];

     echo $Query = "UPDATE students SET name='$Name',technology_id=$TechnologyID,session='$Session',semester='$Semester',shift='$Shift',gender='$Gender' WHERE id=$ID";

     $Result = mysqli_query($Connection,$Query);

     if (!$Result) {
       $ErrorMsg = 'Update Error !';
     }
     else
      header('location:index.php?page=student&SuccessMsg=Update+Successfull');
    }
  }

  $Query = "SELECT * FROM technologies";
  $Technologies  = mysqli_query($Connection,$Query);

 ?>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="row">
            <?php 
            if (isset($SuccessMsg)) {
              echo '<div class="label label-success">'.$SuccessMsg.'</div>';
            }


            if (isset($ErrorMsg)) {
              echo '<div class="label label-warning">'.$ErrorMsg.'</div>';
            }

           ?>
          </div>
          <a href="index.php?page=student" class="btn btn-primary">Back</a>
          <h1 class="page-header">Edit Student</h1>

          <form action="index.php?page=student_edit" method="POST" enctype="multipart/form-data" class="form form-horizontal">
            <input type="hidden" name="id" value="<?php echo $Student['id']; ?>">
            <div class="form-group">
              <label for="name" class="control-label col-md-2">Name</label>
              <div class="col-md-10">
                <input type="text" name="name" class="form-control" value="<?php echo $Student['name']; ?>">
              </div>
            </div>

            <div class="form-group">
              <label for="technology" class="control-label col-md-2">Technology</label>
              <div class="col-md-10">
                <select name="technology_id" id="" class="form-control" value="<?php echo $Student['technology_id'] ?>">
                  <option value="">Select Technology</option>
                  <?php while($Technology = mysqli_fetch_assoc($Technologies)){ ?>
                    <option value="<?php echo $Technology['id']; ?>"><?php echo $Technology['name']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="semester" class="control-label col-md-2">Semester</label>
              <div class="col-md-10">
                <select name="semester" id="semester" class="form-control" value="<?php echo $Student['semester']; ?>">
                  <option value="">Select Semester</option>
                  <?php for($i = 1; $i <= 8; $i++){?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?> Semester</option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="shift" class="control-label col-md-2">Shift</label>
              <div class="col-md-10">
                <select name="shift" class="form-control" id="" value="<?php echo $Student['shift']; ?>">
                  <option value="">Select Shift</option>
                  <option value="First Shift">First Shift</option>
                  <option value="Second Shift">Second Shift</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="session" class="control-label col-md-2">Session</label>
              <div class="col-md-10">
                <select name="session" id="" class="form-control" value="<?php echo $Student['session']; ?>">
                <?php 
                  $Year = date('Y');
                  $Limit = $Year - 5;

                  for($Year; $Year > $Limit; $Year--){
                    $PreviousYear = $Year -1;
                 ?>
                 <option value="<?php echo $PreviousYear.'-'.$Year ?>"><?php echo $PreviousYear.'-'.$Year ?></option>

               <?php } ?>
               </select>
              </div>
            </div>

            <div class="form-group">
              <label for="gender" class="control-label col-md-2">Gender</label>
              <div class="col-md-10">
                <select name="gender" id="" class="form-control" value="<?php echo $Student['gender'] ?>">
                  <option value="">Select Gender</option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                  <option value="Other">Other</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 col-md-offset-4">
                <input type="submit" name="submit" value="Update Student" class="btn btn-primary btn-block">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
