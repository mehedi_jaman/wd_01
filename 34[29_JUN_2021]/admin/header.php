<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <link rel="canonical" href="https://getbootstrap.com/docs/3.4/examples/dashboard/">

    <title>Admin Panel :: KPI</title>

    <!-- Bootstrap core CSS -->
    <link href="../libs/bootstrap-3.4.1/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../libs/custom/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">KPI Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Dashboard</a></li>
            <li><a href="index.php?page=settings">Settings</a></li>
            <li><a href="index.php?page=profile">Profile</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="index.php?page=student">Students</a></li>
            <li><a href="index.php?page=employee">Employees</a></li>
            <li><a href="index.php?page=principal">Principal</a></li>
            <li><a href="index.php?page=v_principal">Vice principal</a></li>
            <li><a href="index.php?page=technology">Technologies</a></li>
            <li><a href="index.php?page=academic_calender">Academic Calender</a></li>
            <li><a href="index.php?page=class_routine">Class Routine</a></li>
            <li><a href="index.php?page=digital_content">Digital Content</a></li>
            <li><a href="index.php?page=institute">Institutue</a></li>
            <li><a href="index.php?page=notice">Notice</a></li>
            <li><a href="index.php?page=ntvqf">NTVQF</a></li>
            <li><a href="index.php?page=photo_gallery">Photo Gallery</a></li>
            <li><a href="index.php?page=result">Result</a></li>
            <li><a href="index.php?page=slider">Slider</a></li>
            <li><a href="index.php?page=syllabus">Syllabus</a></li>
          </ul>
          
        </div>