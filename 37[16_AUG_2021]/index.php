<?php 

	include('header.php');
	include('sidebar.php');

	$page = $_REQUEST['page'];

	switch ($page) {
		case 'dashboard':
			include('home.php');
			break;
		case 'new':
			include('new.php');
			break;
		case 'edit':
			include('edit.php');
			break;
		case 'list':
			include('list.php');
			break;
		default:
			include('home.php');
			break;
	}

	include('footer.php');


 ?>