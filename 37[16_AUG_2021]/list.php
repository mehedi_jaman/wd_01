<?php 
    include('db.php');

    if(isset($_REQUEST['action'])){
        if ($_REQUEST['action'] == 'delete') {
            $id = $_REQUEST['id'];

            $Query = "DELETE FROM students where id = $id";

            $Result = mysqli_query($Connection,$Query);

            if (!$Result) 
                $errormsg = 'Delete Error';
            else
                $successmsg = "Delete Successfull !";
            
        }
    }

    $Query = "SELECT * FROM students";
    $Result = mysqli_query($Connection,$Query);

 ?>
        
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Student List</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Student List</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                <?php 
                                    if (isset($errormsg)) { ?>
                                        <span class="label label-danger"><?php echo $errormsg ?></span>
                                <?php 
                                        
                                    }
                                 ?>

                                 <?php 
                                    if (isset($successmsg)) { ?>
                                        <span class="label label-success"><?php echo $successmsg ?></span>
                                <?php 
                                        
                                    }
                                 ?>
                            </div>
                            <div class="card-body">
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Class Roll</th>
                                            <th>Board Roll</th>
                                            <th>Reg No</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php while($Row = mysqli_fetch_array($Result,MYSQLI_ASSOC)){ ?>
                                            <tr>
                                                <td> <?php echo $Row['name']; ?> </td>
                                                <td> <?php echo $Row['class_roll']; ?> </td>
                                                <td> <?php echo $Row['board_roll']; ?> </td>
                                                <td> <?php echo $Row['reg_no']; ?> </td>
                                                <td> <?php echo $Row['phone']; ?> </td>
                                                <td>
                                                    <a href="index.php?page=edit&action=edit&id=<?php echo $Row['id']; ?>" class="btn btn-warning">Edit</a>
                                                    <a href="index.php?page=list&action=delete&id=<?php echo $Row['id']; ?>" class="btn btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </main>