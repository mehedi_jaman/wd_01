<?php 
    include('db.php');

    if(isset($_POST['submit'])){
        if ($_POST['submit'] == 'Update Student') {
            $id         = $_REQUEST['id'];
            $name       = $_POST['name'];
            $class_roll = $_POST['class_roll'];
            $board_roll = $_POST['board_roll'];
            $reg_no     = $_POST['reg_no'];
            $phone      = $_POST['phone'];

            $Query = "UPDATE students SET name = '$name',class_roll = '$class_roll',board_roll = '$board_roll',reg_no = '$reg_no', phone = '$phone' WHERE id = $id";

            $Result = mysqli_query($Connection,$Query);

            if(!$Result)
                $errormsg = 'Update Error !';
            else
                $successmsg = 'Student Updated Successfully !';

        }
    }

    if(isset($_REQUEST['action'])){
        if ($_REQUEST['action'] == 'edit') {
            $id = $_REQUEST['id'];

            $Query = "SELECT * FROM students where id = $id";

            $Result = mysqli_query($Connection,$Query);

            $Result = mysqli_fetch_assoc($Result);
        }
    }
    
 ?>
        
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">Edit Student</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Edit Student</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                <?php 
                                    if (isset($errormsg)) { ?>
                                        <span class="label label-danger"><?php echo $errormsg ?></span>
                                <?php 
                                        
                                    }
                                 ?>

                                 <?php 
                                    if (isset($successmsg)) { ?>
                                        <span class="label label-success"><?php echo $successmsg ?></span>
                                <?php 
                                        
                                    }
                                 ?>

                            </div>
                            <div class="card-body">
                                <form action="<?php echo $_SERVER['SELF']; ?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="name" class="control-label col-md-2">Name</label>
                                        <div class="col-md-4">
                                            <input type="text" name="name" value="<?php echo $Result['name']; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="class_roll" class="control-label col-md-2">Class roll</label>
                                        <div class="col-md-4">
                                            <input type="text" name="class_roll" value="<?php echo $Result['class_roll']; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="board_roll" class="control-label col-md-2">Board roll</label>
                                        <div class="col-md-4">
                                            <input type="text" name="board_roll" value="<?php echo $Result['board_roll']; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="reg_no" class="control-label col-md-2">Reg No</label>
                                        <div class="col-md-4">
                                            <input type="text" name="reg_no" value="<?php echo $Result['reg_no']; ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone" class="control-label col-md-2">Phone</label>
                                        <div class="col-md-4">
                                            <input type="text" name="phone" value="<?php echo $Result['phone']; ?>">
                                        </div>
                                    </div>

                                    <input type="submit" name="submit" value="Update Student">
                                    <a href="index.php?page=list" class='btn btn-primary'>Back to List</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </main>