<?php 
    include('db.php');

    if (isset($_POST['submit'])) {
        if ($_POST['submit'] == 'Add new Student') {
            $name       = $_POST['name'];
            $class_roll = $_POST['class_roll'];
            $board_roll = $_POST['board_roll'];
            $reg_no     = $_POST['reg_no'];
            $phone      = $_POST['phone'];

            $Query = "INSERT INTO students (name, class_roll,board_roll, reg_no, phone) VALUES('$name','$class_roll','$board_roll','$reg_no','$phone')";

            $Result = mysqli_query($Connection,$Query);

            if (!$Result) {
                $errormsg = "Data Insertion Failed !";
            }
            else
                $successmsg = 'New Student Added Successfully !';
        }
    }
 ?>
        
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h1 class="mt-4">New Student</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">New Student</li>
                        </ol>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                <?php 
                                    if (isset($errormsg)) { ?>
                                        <span class="label label-danger"><?php echo $errormsg ?></span>
                                <?php 
                                        
                                    }
                                 ?>

                                 <?php 
                                    if (isset($successmsg)) { ?>
                                        <span class="label label-success"><?php echo $successmsg ?></span>
                                <?php 
                                        
                                    }
                                 ?>

                            </div>
                            <div class="card-body">
                                <form action="<?php echo $_SERVER['SELF']; ?>" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="name" class="control-label col-md-2">Name</label>
                                        <div class="col-md-4">
                                            <input type="text" name="name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="class_roll" class="control-label col-md-2">Class roll</label>
                                        <div class="col-md-4">
                                            <input type="text" name="class_roll">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="board_roll" class="control-label col-md-2">Board roll</label>
                                        <div class="col-md-4">
                                            <input type="text" name="board_roll">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="reg_no" class="control-label col-md-2">Reg No</label>
                                        <div class="col-md-4">
                                            <input type="text" name="reg_no">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone" class="control-label col-md-2">Phone</label>
                                        <div class="col-md-4">
                                            <input type="text" name="phone">
                                        </div>
                                    </div>

                                    <input type="submit" name="submit" value="Add new Student">
                                </form>
                            </div>
                        </div>
                    </div>
                </main>